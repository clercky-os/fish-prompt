package main

import (
	"fmt"
	"os"
	"strings"
)

const (
	insert = "insert"
	normal = "default"
	replace = "replace_one"
	visual = "visual"
)

func main() {
	// Arguments: [name program] [fish_bind_mode] [proc status] [git status]

	// Vim status
	var status string = ""
	switch os.Args[1] {
	case insert:
		status = "\033[36m[I]"
	case normal:
		status = "\033[31m[N]"
	case replace:
		status = "\033[34m[R]"
	case visual:
		status = "\033[33m[V]"
	default:
		status = "\033[0m[?]"
	}

	// Git status
	gitStatus := ""
	if len(os.Args) >= 4 {
		gitStatus = os.Args[3]
	} else {
		gitStatus = ""
	}

	// current user
	cuser := os.Getenv("USER")

	// CWD
	cwd := os.Getenv("PWD")
	cwd = func (cwd string) string {
		cwd = strings.Replace(cwd, os.Getenv("HOME"), "~", 1)
		cwds := strings.Split(cwd, "/")
		for i:= 0; i < len(cwds)-1; i++ {
			if len(cwds[i]) == 0 { continue; }
			cwds[i] = string(cwds[i][0])
		}
		return strings.Join(cwds, "/")
	}(cwd)

	// Prompt char
	promptChar := ""
	switch os.Args[2] {
	case "0":
		promptChar += "\033[37m"
	default:
		promptChar += "\033[31m"
	}
	switch os.Getuid() {
	case 0:
		promptChar += "# "
	default:
		promptChar += "λ "
	}

	// Define colors
	white := "\033[37m"
	vi_color := "\033[31m"
	orange := "\033[33m"
	limegreen := "\033[36m"
	normal := "\033[0m"

	// Render all
	fmt.Println(white+"╭─"+vi_color+status+white+"-"+orange+
				cuser+white+" in "+limegreen+cwd+
				" "+gitStatus+normal)
	fmt.Print(white+"╰─"+promptChar+normal)

}

