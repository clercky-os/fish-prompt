.DEFAULT_GOAL := all

all: build

build: *.go
	@echo "Build program"
	go build -gcflags="-N -l" -o a.out

clean:
	@echo "Cleaning binaries"
	rm a.out

install:
	@echo "Installing binary"
	go install .
