# Fish Prompt

## Building and installing

To build:
```bash
$ make
```

To install
```bash
$ make install
```
## Dependencies

```bash
$ sudo pacman install go
```

## Usage

General use
```bash
$ fish-prompt [vi bind mode] [exit status] [git status]
```

Use in config.fish
```fish
fish_vi_key_bindings # Enable fish vi

# Disable default show beheavior
function fish_mode_prompt
end

# Add prompt to fish
function fish_prompt
	set -g __fish_git_prompt_char_stateseparator ' '
	set -g __fish_git_prompt_color 5fdfff
	set -g __fish_git_prompt_color_flags df5f00
	set -g __fish_git_prompt_color_prefix white
	set -g __fish_git_prompt_color_suffix white
	set -g __fish_git_prompt_showdirtystate true
	set -g __fish_git_prompt_showuntrackedfiles true
	set -g __fish_git_prompt_showstashstate true
	set -g __fish_git_prompt_show_informative_status true 

	fish-prompt $fish_bind_mode $status (__fish_git_prompt " (%s)")
end
```

## Features

* [x] Working prototype looking like normal lambda prompt
* [x] Make paths shorter to look ok in small terminal screens
* [ ] Generate own git status instead of relying on fish
